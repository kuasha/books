

Big-Oh notation (upper bound of a function)


-----------------------------------------------
O(f(n)) is the set of all functions T(n) that satisfy that there exist a positive constant c and N such that for all n>=N

T(n) <= cf(n)

-----------------------------------------------

Bog-Oh notation does not account for constant factors. Instead of writing O(2000n) it is sufficient to write O(n).

Bog-Oh notation usually account for fastest growing term. 

It is not required to write O(n+n^3+n^5). We just write O(n^5) since N^5 is fastest growing term. 

If a function is in O(n^2) is is also in O(n^3) since b-g-oh notation only for upper bound.


Big-Omega notation (lower bound of a function)

-----------------------------------------------
Omega(f(n)) is the set of all functions T(n) that satisfy that there exist a positive constant d and N such that for all n>=N

T(n) >= df(n)

-----------------------------------------------


Big theta

